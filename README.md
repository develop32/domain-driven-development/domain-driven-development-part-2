# ドメイン駆動開発パート２

ドメイン駆動開発パート２



## プロジェクトの依存関係

```mermaid
graph BT
	WinForm --> Infrastructure --> Domain
	WinForm --> Domain
	Tests
```

- **WinForm**
  画面に関する処理。
  InfrastructureとDomainを参照する。
- **Infrastructure**
  アプリケーションの外部とのやりとり。
  Domainを参照する。
- **Domain**
  業務仕様、ビジネスロジック。
  参照無し。
- Tests
  WinForm、Infrastructure、Domain全てを参照する。

※ ドメイン駆動ではApplication層をWinFormとInfrastructureの間に設けられることもある。



プロジェクトを分割する理由

1. 可読性を上げる
   各プロジェクトにはそれぞれ意味があり、どこに何があるのか分かりやすい。
2. 変化に強い
   WinForm→WPF→UWP、Oracle→SqlServer、Oracle Ver10→Ver11



## フォルダ構成

### **Domain**：業務仕様、ビジネスロジックなど

- <font color="red">**Entities**</font>
  一意なデータの塊。テーブルの一行。行の中で完結するロジックの置き場。
- Exceptions
  Exceptionを継承した例外クラスの集まり。基本的に、異常は例外で表す。戻り値をBooleanにして判断しない。呼び元に`If`を書かせない。値が戻ったら正常。エラー時はCatch！バケツリレーはしない。
  例外処理は負荷が高い。その為、例外処理で処理を終了させずにFor文で繰り替えし行うのはNG。
- Helpers
  Staticでどこにあってもいいもの。害がないもの。考え方としてはマイクロソフトの.NetFrameworkで作ってくれてたら良かったもの。
- <font color="red">**Repositories**</font>
  アプリケーションの外部と接触する部分には基本的にインターフェイスを作る。テスト容易性のため。
  DBから取得した値を加工したい場合、具象クラスを作成する。
- <font color="red">**ValueObjects**</font>
  値として扱うクラス。数値の「3」等はそのまま扱うとビジネスロジックが散らばるため。ValueObjectの「3」として扱う。テーブルの列のValueObject化が有効。
- StaticValues
  マスタデータなど都度DBから取得せずに、値をキャッシュとして保持する場合に利用。アプリケーション起動時にデータを取得して保持しておく。
- Logics
  Staticでビジネスロジック、業務ロジックを含むもの。分析、診断ロジックなど。
- Modules
  - どこにも収まらないもの。
  - 基本的にロジックはEntity、ValueObjectに入れるが、入れれないもの。
  - 独立したロジックでもないもの。



### **Infrastructure**：アプリケーションの外部とのやりとり

なぜ外部との接触部分を集めるのか？→外部の影響を局所化し修正しやすくすることと、テスト容易性。

Domain層のRepositoryにあるインターフェイスを使う。

フォルダ構成はテクノロジーごとにフォルダーを分ける。

Factoriesを利用する場合Friendにする。

- Oracle
- SQLServer
- CSV
- 外部機器
- Fake



### **WinForm**：UI層

ビジネスロジックとの分離。
　MVVM(Model-View-ViewModel)パターンを適用。Modelは今回ではDomain層とInfrastructure層。

- Views
  - 画面。
- ViewModels
  - 基本的には、ドメイン層やインフラストラクチャ層の機能を呼び出す。
  - トランザクション処理はここに実装。
    - Infrastructure層では融通がきかない為、使う側のViewModel。
- BackgroundWorkers
  - タイマーイベント。理由は、保守性を上げるため。Domainで定期的に動いていると探しにくい。

※ ViewとViewModelは基本的に1対1。



### **Tests**：テストコード

- 本番(Domain、Infrastructure、WinForm)コードのフォルダに`Tests`をつけたフォルダ。
  - ViewModelTestsなど
- 基本的にViewModelに対してテストを書けばOK。
  - ViewModelから様々な処理を呼び出され、殆どの処理に対してテストが行われるため。
- Infrastructureのテスト
  - 不要
    - 実機がないとできない為。
    - 実機があっても前準備、後片付け等が必要であり、コストに見合うリターンがない？
  - Moqで代用
- 画面のテスト
  - 不要
    - ViewModelに対してテスト
    - 実際の画面は目で見てバインドされているかテストする
- できるだけInfrastructure層やWinForm層はシンプルにしてロジックを書かない。
  - テストを行わない為。



## 全体像

```mermaid
graph TD
	値 -->|アプリに一つの値| Static
	Static -->|マスタなどのDBの値やリスト| StaticValues
	Static -->|単体のアプリやユーザ情報| Shared
	値 -->|処理の度に生成| EntityOrValueObject
	EntityOrValueObject[Entity or ValueOect]
	EntityOrValueObject -->|テーブルの一行| Entity
	EntityOrValueObject -->|テーブルのカラム| ValueObject
	logic -->|データと一体型でそのデータ専用| EntityOrValueObject
	logic -->|ビジネスロジック| Logics
	logic -->|一般仕様| Helpers
```

