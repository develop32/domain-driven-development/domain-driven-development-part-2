﻿Imports NDDD.Domain.ValueObjects

Namespace Entities
    ''' <summary>
    ''' 計測エンティティ
    ''' </summary>
    Public Class MeasureEntity

        ''' <summary>エリアID</summary>
        Public ReadOnly Property AreaId As AreaId
        ''' <summary>計測日時</summary>
        Public ReadOnly Property MeasureDate As MeasureDate
        ''' <summary>計測値</summary>
        Public ReadOnly Property MeasureValue As MeasureValue

        ''' <summary>
        ''' 今月で猛暑日
        ''' </summary>
        ''' <returns>Trueの場合今月で猛暑日</returns>
        Public ReadOnly Property IsVeryHotDayOfThisMonth() As Boolean
            Get
                If MeasureValue.IsIntenseHeat AndAlso MeasureDate.IsThisMonth Then
                    Return True
                End If
                Return False
            End Get
        End Property

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="areaId">エリアID</param>
        ''' <param name="MeasureDate">計測日時</param>
        ''' <param name="MeasureValue">計測値</param>
        Public Sub New(areaId As Integer,
                       MeasureDate As Date,
                       MeasureValue As Decimal)
            Me.AreaId = New AreaId(areaId)
            Me.MeasureDate = New MeasureDate(MeasureDate)
            Me.MeasureValue = New MeasureValue(MeasureValue)
        End Sub

    End Class

End Namespace