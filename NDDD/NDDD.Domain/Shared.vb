﻿Imports System.Configuration

''' <summary>
''' Shared
''' </summary>
Public Class [Shared]
    ''' <summary>
    ''' Fakeの時True
    ''' </summary>
    Public Shared ReadOnly IsFake As Boolean =
        ConfigurationManager.AppSettings("IsFake") = "1"
    ''' <summary>
    ''' Fakeのパス
    ''' </summary>
    Public Shared ReadOnly FakePath As String =
        ConfigurationManager.AppSettings("FakePath")
    ''' <summary>
    ''' ログインID
    ''' </summary>
    ''' <returns></returns>
    Public Shared Property LoginId As String = String.Empty
End Class
