﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Module DecimalHelper
    <Extension>
    Public Function RoundString(value As Decimal, decimalPoint As Integer) As String
        value = Convert.ToDecimal(Math.Round(value, decimalPoint))
        Return value.ToString(
            If(decimalPoint = 0,
               String.Empty,
               "." & String.Concat(Enumerable.Repeat("0", decimalPoint))))
    End Function
End Module
