﻿Imports NDDD.Domain.Entities

Namespace Repositories
    ''' <summary>
    ''' 計測リポジトリー
    ''' </summary>
    Public Interface IMeasureRepository
        ''' <summary>
        ''' 直近値取得
        ''' </summary>
        ''' <returns></returns>
        Function GetLatest() As MeasureEntity
        ''' <summary>
        ''' エリアごとの直近値取得
        ''' </summary>
        ''' <returns></returns>
        Function GetLatests() As IReadOnlyList(Of MeasureEntity)
    End Interface

End Namespace