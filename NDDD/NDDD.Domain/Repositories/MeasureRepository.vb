﻿Imports System.Threading
Imports NDDD.Domain.Entities
Imports NDDD.Domain.Exceptions

Namespace Repositories
    ''' <summary>
    ''' 計測リポジトリー(具象クラス)
    ''' </summary>
    Public NotInheritable Class MeasureRepository
        Private ReadOnly _repository As IMeasureRepository

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="repository">計測リポジトリー</param>
        Public Sub New(repository As IMeasureRepository)
            _repository = repository
        End Sub

        ''' <summary>
        ''' 直近値の取得。
        ''' 3回計測した平均値を返す。
        ''' DBから取得した値を加工したい場合
        ''' </summary>
        ''' <returns></returns>
        Public Function GetLatest() As MeasureEntity
            Dim val1 As MeasureEntity = _repository.GetLatest()
            If val1 Is Nothing Then
                Throw New DataNotExistsException()
            End If
            Thread.Sleep(1000)
            Dim val2 As MeasureEntity = _repository.GetLatest()
            Thread.Sleep(1000)
            Dim val3 As MeasureEntity = _repository.GetLatest()
            Dim sum As Decimal = val1.MeasureValue.Value + val2.MeasureValue.Value + val3.MeasureValue.Value
            Dim ave As Decimal = sum / 3D
            Return New MeasureEntity(val3.AreaId.Value, val3.MeasureDate.Value, ave)
        End Function
    End Class

End Namespace