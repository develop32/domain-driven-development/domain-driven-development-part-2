﻿Imports System.Runtime.CompilerServices

Namespace Helpers

    ''' <summary>
    ''' Decimalヘルパー
    ''' </summary>
    Module DecimalHelper
        ''' <summary>
        ''' 指定した桁数で四捨五入し、ゼロを埋める。
        ''' </summary>
        ''' <param name="value">値</param>
        ''' <param name="decimalPoint">小数点以下桁数</param>
        ''' <returns></returns>
        <Extension>
        Public Function RoundString(value As Decimal, decimalPoint As Integer) As String
            value = Convert.ToDecimal(Math.Round(value, decimalPoint))
            Return value.ToString(
                If(decimalPoint = 0,
                   String.Empty,
                   "." & String.Concat(Enumerable.Repeat("0", decimalPoint))))
        End Function
    End Module

End Namespace