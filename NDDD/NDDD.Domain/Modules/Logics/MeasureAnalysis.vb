﻿''' <summary>
''' 計測分析
''' </summary>
Public Class MeasureAnalysis
    ''' <summary>
    ''' 実行
    ''' </summary>
    ''' <param name="value1">値1</param>
    ''' <param name="value2">値2</param>
    ''' <param name="value3">値3</param>
    ''' <param name="value4">値4</param>
    ''' <param name="value5">値5</param>
    ''' <returns></returns>
    Public Shared Function Execute(value1 As Decimal,
                                   value2 As Decimal,
                                   value3 As Decimal,
                                   value4 As Decimal,
                                   value5 As Decimal) As Decimal
        Dim sumValue As Decimal = value1 + value2 + value3 + value4 + value5
        Dim aveValue As Decimal = sumValue / 5
        Return aveValue
    End Function
End Class
