﻿Imports NDDD.Domain.Entities
Imports NDDD.Domain.Repositories
Imports NDDD.Domain.ValueObjects

Namespace StaticValues
    ''' <summary>
    ''' 計測値のリストクラス
    ''' </summary>
    Public Class Measures
        ''' <summary>
        ''' エリアごとの直近値のリスト
        ''' </summary>
        Private Shared ReadOnly _entities As New List(Of MeasureEntity)

        ''' <summary>
        ''' リストの作成
        ''' </summary>
        ''' <param name="repository">計測リポジトリー</param>
        Public Shared Sub Create(repository As IMeasureRepository)
            SyncLock (CType(_entities, ICollection)).SyncRoot
                _entities.Clear()
                _entities.AddRange(repository.GetLatests())
            End SyncLock
        End Sub

        Public Shared Sub Add(entity As MeasureEntity)
            SyncLock (CType(_entities, ICollection)).SyncRoot
                _entities.Add(entity)
            End SyncLock
        End Sub

        ''' <summary>
        ''' 直近値の取得(エリアID指定)
        ''' </summary>
        ''' <param name="areaId">エリアID</param>
        ''' <returns></returns>
        Public Shared Function GetLatest(areaId As AreaId) As MeasureEntity
            SyncLock (CType(_entities, ICollection)).SyncRoot
                Return _entities.Find(Function(x) x.AreaId = areaId)
            End SyncLock
        End Function
    End Class

End Namespace