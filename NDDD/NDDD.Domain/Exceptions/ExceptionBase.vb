﻿Namespace Exceptions
    ''' <summary>
    ''' 例外基底クラス
    ''' </summary>
    Public MustInherit Class ExceptionBase
        Inherits Exception


        Public MustOverride ReadOnly Property Kind As ExceptionKind

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="message">例外メッセージ</param>
        Public Sub New(message As String)
            MyBase.New(message)
        End Sub

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="message">例外メッセージ</param>
        ''' <param name="exception">元になった例外</param>
        Public Sub New(message As String, exception As Exception)
            MyBase.New(message, exception)
        End Sub

        ''' <summary>
        ''' 例外区分
        ''' </summary>
        Public Enum ExceptionKind
            Info
            Warning
            [Error]
        End Enum
    End Class

End Namespace