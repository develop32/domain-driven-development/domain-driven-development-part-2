﻿Namespace Exceptions

    ''' <summary>
    ''' データなし例外
    ''' </summary>
    Public Class DataNotExistsException
        Inherits ExceptionBase

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        Public Sub New()
            MyBase.New("データがありません")
        End Sub

        ''' <summary>
        ''' 区分
        ''' </summary>
        ''' <returns></returns>
        Public Overrides ReadOnly Property Kind As ExceptionKind
            Get
                Return ExceptionKind.Info
            End Get
        End Property
    End Class

End Namespace