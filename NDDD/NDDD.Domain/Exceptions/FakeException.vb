﻿Namespace Exceptions
    ''' <summary>
    ''' Fake例外
    ''' </summary>
    Public Class FakeException
        Inherits ExceptionBase

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="message">例外メッセージ</param>
        ''' <param name="exception">元になった例外</param>
        Public Sub New(message As String, exception As Exception)
            MyBase.New(message & Environment.NewLine & exception.Message, exception)
        End Sub

        ''' <summary>
        ''' 例外区分
        ''' </summary>
        Public Overrides ReadOnly Property Kind As ExceptionKind
            Get
                Return ExceptionKind.Error
            End Get
        End Property
    End Class

End Namespace