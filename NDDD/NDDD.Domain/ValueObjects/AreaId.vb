﻿Namespace ValueObjects

    ''' <summary>
    ''' AreaIdクラス
    ''' </summary>
    Public NotInheritable Class AreaId
        Inherits ValueObject(Of AreaId)

        ''' <summary>値</summary>
        Public ReadOnly Property Value As Integer

        ''' <summary>表示値</summary>
        Public ReadOnly Property DisplayValue As String
            Get
                Return Value.ToString().PadLeft(4, "0"c)
            End Get
        End Property

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="value">値</param>
        Public Sub New(value As Integer)
            Me.Value = value
        End Sub

        ''' <summary>
        ''' 比較
        ''' </summary>
        ''' <param name="other">他のオブジェクト</param>
        ''' <returns>等しい場合True</returns>
        Protected Overrides Function EqualsCore(other As AreaId) As Boolean
            Return Value = other.Value
        End Function
    End Class

End Namespace