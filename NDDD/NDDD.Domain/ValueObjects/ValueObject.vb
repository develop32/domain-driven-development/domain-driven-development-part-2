﻿Namespace ValueObjects

    ''' <summary>
    ''' ValueObjectの基底クラス
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    Public MustInherit Class ValueObject(Of T As ValueObject(Of T))
        ''' <summary>
        ''' Equals
        ''' </summary>
        ''' <param name="obj">obj</param>
        ''' <returns></returns>
        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            Dim vo = TryCast(obj, T)

            If vo Is Nothing Then
                Return False
            End If

            Return EqualsCore(vo)
        End Function

        ''' <summary>
        ''' =
        ''' </summary>
        ''' <param name="vo1">vo1</param>
        ''' <param name="vo2">vo2</param>
        ''' <returns></returns>
        Public Shared Operator =(ByVal vo1 As ValueObject(Of T), ByVal vo2 As ValueObject(Of T)) As Boolean
            Return Equals(vo1, vo2)
        End Operator

        ''' <summary>
        ''' ＜＞
        ''' </summary>
        ''' <param name="vo1">vo1</param>
        ''' <param name="vo2">vo2</param>
        ''' <returns></returns>
        Public Shared Operator <>(ByVal vo1 As ValueObject(Of T), ByVal vo2 As ValueObject(Of T)) As Boolean
            Return Not Equals(vo1, vo2)
        End Operator

        ''' <summary>
        ''' EqualsCore
        ''' </summary>
        ''' <param name="other">other</param>
        ''' <returns></returns>
        Protected MustOverride Function EqualsCore(ByVal other As T) As Boolean

        ''' <summary>
        ''' ToString
        ''' </summary>
        ''' <returns>文字列</returns>
        Public Overrides Function ToString() As String
            Return MyBase.ToString()
        End Function

        ''' <summary>
        ''' GetHashCode
        ''' </summary>
        ''' <returns>値</returns>
        Public Overrides Function GetHashCode() As Integer
            Return MyBase.GetHashCode()
        End Function
    End Class

End Namespace