﻿Imports System.Runtime.InteropServices.WindowsRuntime
Imports NDDD.Domain.Helpers

Namespace ValueObjects

    ''' <summary>
    ''' MeasureValueクラス
    ''' </summary>
    Public Class MeasureValue
        Inherits ValueObject(Of MeasureValue)

        ''' <summary>
        ''' 単位
        ''' </summary>
        Public Const Unit As String = "℃"

        ''' <summary>値</summary>
        Public ReadOnly Property Value As Decimal

        Public ReadOnly Property DisplayValue As String
            Get
                'Return Math.Round(Value, 2) & "℃"
                Return Value.RoundString(4) & Unit
            End Get
        End Property

        ''' <summary>
        ''' 猛暑
        ''' </summary>
        ''' <returns>Trueの場合猛暑</returns>
        Public ReadOnly Property IsIntenseHeat As Boolean
            Get
                Return Value >= 35D
            End Get
        End Property

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="value">値</param>
        Public Sub New(value As Decimal)
            Me.Value = value
        End Sub

        ''' <summary>
        ''' EqualsCore
        ''' </summary>
        ''' <param name="other">比較する値</param>
        ''' <returns>等しい場合True</returns>
        Protected Overrides Function EqualsCore(other As MeasureValue) As Boolean
            Return Value = other.Value
        End Function
    End Class

End Namespace