﻿Namespace ValueObjects

    ''' <summary>
    ''' MeasureDateクラス
    ''' </summary>
    Public Class MeasureDate
        Inherits ValueObject(Of MeasureDate)

        ''' <summary>値</summary>
        Public ReadOnly Property Value As Date

        ''' <summary>表示値</summary>
        Public ReadOnly Property DisplayValue As String
            Get
                Return Value.ToString("yyy/MM/dd HH:mm:ss")
            End Get
        End Property

        ''' <summary>
        ''' 今月
        ''' </summary>
        ''' <returns>Trueの場合今月</returns>
        Public ReadOnly Property IsThisMonth As Boolean
            Get
                Return Value.Year = Now.Year AndAlso Value.Month = Now.Month
            End Get
        End Property

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="value">値</param>
        Public Sub New(value As Date)
            Me.Value = value
        End Sub

        ''' <summary>
        ''' EqualsCore
        ''' </summary>
        ''' <param name="other">比較する値</param>
        ''' <returns>等しい場合True</returns>
        Protected Overrides Function EqualsCore(other As MeasureDate) As Boolean
            Return Value = other.Value
        End Function
    End Class

End Namespace