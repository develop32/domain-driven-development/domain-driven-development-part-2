﻿Imports System.Threading
Imports NDDD.Domain.StaticValues
Imports NDDD.Infrastructure

Namespace BackgroundWorkers
    ''' <summary>
    ''' 直近値のタイマー
    ''' </summary>
    Friend Module LatestTimer
        ''' <summary>
        ''' タイマー
        ''' </summary>
        Private ReadOnly _timer As Timer

        ''' <summary>
        ''' Trueの場合処理中
        ''' </summary>
        Private _isWork As Boolean = False

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        Sub New()
            _timer = New Timer(AddressOf Callback)
        End Sub

        ''' <summary>
        ''' タイマー開始
        ''' </summary>
        Friend Sub Start()
            _timer.Change(0, 10000)
        End Sub

        ''' <summary>
        ''' タイマー停止
        ''' </summary>
        Friend Sub [Stop]()
            _timer.Change(Timeout.Infinite, Timeout.Infinite)
        End Sub

        ''' <summary>
        ''' コールバック
        ''' </summary>
        ''' <param name="o">オブジェクト</param>
        Private Sub Callback(ByVal o As Object)
            If _isWork Then
                Return
            End If

            Try
                _isWork = True
                ' 処理
                Measures.Create(Factories.CreateMeasure())
            Finally
                ' Config→Sharedで取得可能にしておく
                '    _timer.Change(10000, 10000)
                _isWork = False
            End Try
        End Sub
    End Module

End Namespace