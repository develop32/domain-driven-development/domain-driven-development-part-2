﻿Imports NDDD.Domain
Imports NDDD.WinForm.BackgroundWorkers

Namespace Views

    Public Class LoginView
        Private Sub LoginButton_Click(sender As Object, e As EventArgs) Handles LoginButton.Click
            [Shared].LoginId = LoginIdTextBox.Text
            Using latestView As New LatestView
                latestView.ShowDialog()
            End Using
        End Sub

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="sender">コントロール</param>
        ''' <param name="e">イベント引数</param>
        Private Sub LoginView_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            LatestTimer.Start()
        End Sub
    End Class

End Namespace