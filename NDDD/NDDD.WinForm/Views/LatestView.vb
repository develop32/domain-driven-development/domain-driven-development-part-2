﻿Imports NDDD.Domain.Exceptions
Imports NDDD.WinForm.ViewModels

Namespace Views
    ''' <summary>
    ''' 直近値View
    ''' </summary>
    Public Class LatestView
        ''' <summary>直近値ViewModel</summary>
        Private ReadOnly _viewModel As New LatestViewModel()

        ''' <summary>コンストラクタ</summary>
        Public Sub New()

            ' この呼び出しはデザイナーで必要です。
            InitializeComponent()

            ' InitializeComponent() 呼び出しの後で初期化を追加します。
            AreaIdTextBox.DataBindings.Add("Text", _viewModel, NameOf(_viewModel.AreaIdText))
            MeasureDateTextBox.DataBindings.Add("Text", _viewModel, NameOf(_viewModel.MeasureDateText))
            MeasureValueTextBox.DataBindings.Add("Text", _viewModel, NameOf(_viewModel.MeasureValueText))

        End Sub

        ''' <summary>
        ''' 検索ボタンクリックイベント
        ''' </summary>
        ''' <param name="sender">コントロール</param>
        ''' <param name="e">イベント引数</param>
        Private Sub SearchButton_Click(sender As Object, e As EventArgs) Handles SearchButton.Click
            Try
                _viewModel.Search()
            Catch ex As Exception
                ExceptionProc(ex)
            End Try
        End Sub
    End Class

End Namespace