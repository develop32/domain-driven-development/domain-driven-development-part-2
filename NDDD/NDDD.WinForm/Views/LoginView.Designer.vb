﻿Namespace Views

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class LoginView
        Inherits BaseForm

        'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Windows フォーム デザイナーで必要です。
        Private components As System.ComponentModel.IContainer

        'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
        'Windows フォーム デザイナーを使用して変更できます。  
        'コード エディターを使って変更しないでください。
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim AreaIdTextLabel As System.Windows.Forms.Label
            Dim MeasureDateTextLabel As System.Windows.Forms.Label
            Me.LoginButton = New System.Windows.Forms.Button()
            Me.LoginIdTextBox = New System.Windows.Forms.TextBox()
            Me.PasswordTextBox = New System.Windows.Forms.TextBox()
            AreaIdTextLabel = New System.Windows.Forms.Label()
            MeasureDateTextLabel = New System.Windows.Forms.Label()
            Me.SuspendLayout()
            '
            'AreaIdTextLabel
            '
            AreaIdTextLabel.AutoSize = True
            AreaIdTextLabel.Location = New System.Drawing.Point(64, 62)
            AreaIdTextLabel.Name = "AreaIdTextLabel"
            AreaIdTextLabel.Size = New System.Drawing.Size(54, 12)
            AreaIdTextLabel.TabIndex = 8
            AreaIdTextLabel.Text = "ログインID:"
            '
            'MeasureDateTextLabel
            '
            MeasureDateTextLabel.AutoSize = True
            MeasureDateTextLabel.Location = New System.Drawing.Point(64, 87)
            MeasureDateTextLabel.Name = "MeasureDateTextLabel"
            MeasureDateTextLabel.Size = New System.Drawing.Size(54, 12)
            MeasureDateTextLabel.TabIndex = 10
            MeasureDateTextLabel.Text = "パスワード:"
            '
            'LoginButton
            '
            Me.LoginButton.Location = New System.Drawing.Point(66, 146)
            Me.LoginButton.Name = "LoginButton"
            Me.LoginButton.Size = New System.Drawing.Size(211, 23)
            Me.LoginButton.TabIndex = 12
            Me.LoginButton.Text = "ログイン"
            Me.LoginButton.UseVisualStyleBackColor = True
            '
            'LoginIdTextBox
            '
            Me.LoginIdTextBox.Location = New System.Drawing.Point(125, 59)
            Me.LoginIdTextBox.Name = "LoginIdTextBox"
            Me.LoginIdTextBox.Size = New System.Drawing.Size(152, 19)
            Me.LoginIdTextBox.TabIndex = 9
            '
            'PasswordTextBox
            '
            Me.PasswordTextBox.Location = New System.Drawing.Point(125, 84)
            Me.PasswordTextBox.Name = "PasswordTextBox"
            Me.PasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
            Me.PasswordTextBox.Size = New System.Drawing.Size(152, 19)
            Me.PasswordTextBox.TabIndex = 11
            '
            'LoginView
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(341, 229)
            Me.Controls.Add(Me.LoginButton)
            Me.Controls.Add(AreaIdTextLabel)
            Me.Controls.Add(Me.LoginIdTextBox)
            Me.Controls.Add(MeasureDateTextLabel)
            Me.Controls.Add(Me.PasswordTextBox)
            Me.Name = "LoginView"
            Me.Text = "LoginView"
            Me.Controls.SetChildIndex(Me.PasswordTextBox, 0)
            Me.Controls.SetChildIndex(MeasureDateTextLabel, 0)
            Me.Controls.SetChildIndex(Me.LoginIdTextBox, 0)
            Me.Controls.SetChildIndex(AreaIdTextLabel, 0)
            Me.Controls.SetChildIndex(Me.LoginButton, 0)
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

        Friend WithEvents LoginButton As Button
        Friend WithEvents LoginIdTextBox As TextBox
        Friend WithEvents PasswordTextBox As TextBox
    End Class

End Namespace