﻿Imports NDDD.Domain
Imports NDDD.Domain.Exceptions

Namespace Views
    ''' <summary>
    ''' Viewの基底クラス
    ''' </summary>
    Public Class BaseForm
        ''' <summary>
        ''' ログ
        ''' </summary>
        Private ReadOnly _logger As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        Public Sub New()

            ' この呼び出しはデザイナーで必要です。
            InitializeComponent()

            ' InitializeComponent() 呼び出しの後で初期化を追加します。
            DebugModeToolStripStatusLabel.Visible = False
#If DEBUG Then
            DebugModeToolStripStatusLabel.Visible = True
#End If
            UserIdToolStripStatusLabel.Text = [Shared].LoginId
        End Sub

        ''' <summary>
        ''' 例外共通処理
        ''' </summary>
        ''' <param name="ex">例外</param>
        Protected Sub ExceptionProc(ByVal ex As Exception)
            _logger.Error(ex.Message, ex)
            Dim icon As MessageBoxIcon = MessageBoxIcon.Error
            Dim caption As String = "エラー"
            Dim exceptionBase = TryCast(ex, ExceptionBase)
            If exceptionBase IsNot Nothing Then
                If exceptionBase.Kind = ExceptionBase.ExceptionKind.Info Then
                    caption = "情報"
                    icon = MessageBoxIcon.Information
                ElseIf exceptionBase.Kind = ExceptionBase.ExceptionKind.Warning Then
                    caption = "警告"
                    icon = MessageBoxIcon.Warning
                End If
            End If

            MessageBox.Show(ex.Message, caption, MessageBoxButtons.OK, icon)
        End Sub

        ''' <summary>
        ''' フォームロードイベント
        ''' </summary>
        ''' <param name="sender">コントロール</param>
        ''' <param name="e">イベント引数</param>
        Private Sub BaseForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            _logger.Info("Open: " & Me.Name)
        End Sub

        ''' <summary>
        ''' フォームクローズイベント
        ''' </summary>
        ''' <param name="sender">コントロール</param>
        ''' <param name="e">イベント引数</param>
        Private Sub BaseForm_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
            _logger.Info("Close: " & Me.Name)
        End Sub
    End Class

End Namespace