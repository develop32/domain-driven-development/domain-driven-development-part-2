﻿Namespace Views

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class BaseForm
        Inherits System.Windows.Forms.Form

        'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Windows フォーム デザイナーで必要です。
        Private components As System.ComponentModel.IContainer

        'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
        'Windows フォーム デザイナーを使用して変更できます。  
        'コード エディターを使って変更しないでください。
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
            Me.DebugModeToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
            Me.UserIdToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
            Me.StatusStrip1.SuspendLayout()
            Me.SuspendLayout()
            '
            'StatusStrip1
            '
            Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DebugModeToolStripStatusLabel, Me.UserIdToolStripStatusLabel})
            Me.StatusStrip1.Location = New System.Drawing.Point(0, 298)
            Me.StatusStrip1.Name = "StatusStrip1"
            Me.StatusStrip1.Size = New System.Drawing.Size(332, 22)
            Me.StatusStrip1.TabIndex = 9
            Me.StatusStrip1.Text = "StatusStrip1"
            '
            'DebugModeToolStripStatusLabel
            '
            Me.DebugModeToolStripStatusLabel.BackColor = System.Drawing.Color.Red
            Me.DebugModeToolStripStatusLabel.Name = "DebugModeToolStripStatusLabel"
            Me.DebugModeToolStripStatusLabel.Size = New System.Drawing.Size(68, 17)
            Me.DebugModeToolStripStatusLabel.Text = "デバッグモード"
            '
            'UserIdToolStripStatusLabel
            '
            Me.UserIdToolStripStatusLabel.Name = "UserIdToolStripStatusLabel"
            Me.UserIdToolStripStatusLabel.Size = New System.Drawing.Size(119, 17)
            Me.UserIdToolStripStatusLabel.Text = "ToolStripStatusLabel1"
            '
            'BaseForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(332, 320)
            Me.Controls.Add(Me.StatusStrip1)
            Me.Name = "BaseForm"
            Me.Text = "BaseForm"
            Me.StatusStrip1.ResumeLayout(False)
            Me.StatusStrip1.PerformLayout()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

        Friend WithEvents StatusStrip1 As StatusStrip
        Friend WithEvents DebugModeToolStripStatusLabel As ToolStripStatusLabel
        Friend WithEvents UserIdToolStripStatusLabel As ToolStripStatusLabel
    End Class

End Namespace