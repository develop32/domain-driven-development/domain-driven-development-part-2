﻿Namespace Views

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class LatestView
        Inherits BaseForm

        'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Windows フォーム デザイナーで必要です。
        Private components As System.ComponentModel.IContainer

        'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
        'Windows フォーム デザイナーを使用して変更できます。  
        'コード エディターを使って変更しないでください。
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim AreaIdTextLabel As System.Windows.Forms.Label
            Dim MeasureDateTextLabel As System.Windows.Forms.Label
            Dim MeasureValueTextLabel As System.Windows.Forms.Label
            Me.AreaIdTextBox = New System.Windows.Forms.TextBox()
            Me.MeasureDateTextBox = New System.Windows.Forms.TextBox()
            Me.MeasureValueTextBox = New System.Windows.Forms.TextBox()
            Me.SearchButton = New System.Windows.Forms.Button()
            AreaIdTextLabel = New System.Windows.Forms.Label()
            MeasureDateTextLabel = New System.Windows.Forms.Label()
            MeasureValueTextLabel = New System.Windows.Forms.Label()
            Me.SuspendLayout()
            '
            'AreaIdTextLabel
            '
            AreaIdTextLabel.AutoSize = True
            AreaIdTextLabel.Location = New System.Drawing.Point(12, 26)
            AreaIdTextLabel.Name = "AreaIdTextLabel"
            AreaIdTextLabel.Size = New System.Drawing.Size(43, 12)
            AreaIdTextLabel.TabIndex = 1
            AreaIdTextLabel.Text = "エリアID:"
            '
            'MeasureDateTextLabel
            '
            MeasureDateTextLabel.AutoSize = True
            MeasureDateTextLabel.Location = New System.Drawing.Point(12, 51)
            MeasureDateTextLabel.Name = "MeasureDateTextLabel"
            MeasureDateTextLabel.Size = New System.Drawing.Size(55, 12)
            MeasureDateTextLabel.TabIndex = 3
            MeasureDateTextLabel.Text = "計測日時:"
            '
            'MeasureValueTextLabel
            '
            MeasureValueTextLabel.AutoSize = True
            MeasureValueTextLabel.Location = New System.Drawing.Point(12, 76)
            MeasureValueTextLabel.Name = "MeasureValueTextLabel"
            MeasureValueTextLabel.Size = New System.Drawing.Size(43, 12)
            MeasureValueTextLabel.TabIndex = 5
            MeasureValueTextLabel.Text = "計測値:"
            '
            'AreaIdTextBox
            '
            Me.AreaIdTextBox.Location = New System.Drawing.Point(73, 23)
            Me.AreaIdTextBox.Name = "AreaIdTextBox"
            Me.AreaIdTextBox.ReadOnly = True
            Me.AreaIdTextBox.Size = New System.Drawing.Size(152, 19)
            Me.AreaIdTextBox.TabIndex = 2
            '
            'MeasureDateTextBox
            '
            Me.MeasureDateTextBox.Location = New System.Drawing.Point(73, 48)
            Me.MeasureDateTextBox.Name = "MeasureDateTextBox"
            Me.MeasureDateTextBox.ReadOnly = True
            Me.MeasureDateTextBox.Size = New System.Drawing.Size(152, 19)
            Me.MeasureDateTextBox.TabIndex = 4
            '
            'MeasureValueTextBox
            '
            Me.MeasureValueTextBox.Location = New System.Drawing.Point(73, 73)
            Me.MeasureValueTextBox.Name = "MeasureValueTextBox"
            Me.MeasureValueTextBox.ReadOnly = True
            Me.MeasureValueTextBox.Size = New System.Drawing.Size(152, 19)
            Me.MeasureValueTextBox.TabIndex = 6
            '
            'SearchButton
            '
            Me.SearchButton.Location = New System.Drawing.Point(14, 110)
            Me.SearchButton.Name = "SearchButton"
            Me.SearchButton.Size = New System.Drawing.Size(211, 23)
            Me.SearchButton.TabIndex = 7
            Me.SearchButton.Text = "検索"
            Me.SearchButton.UseVisualStyleBackColor = True
            '
            'LatestView
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(237, 165)
            Me.Controls.Add(Me.SearchButton)
            Me.Controls.Add(AreaIdTextLabel)
            Me.Controls.Add(Me.AreaIdTextBox)
            Me.Controls.Add(MeasureDateTextLabel)
            Me.Controls.Add(Me.MeasureDateTextBox)
            Me.Controls.Add(MeasureValueTextLabel)
            Me.Controls.Add(Me.MeasureValueTextBox)
            Me.Name = "LatestView"
            Me.Text = "LatestView"
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

        Friend WithEvents AreaIdTextBox As TextBox
        Friend WithEvents MeasureDateTextBox As TextBox
        Friend WithEvents MeasureValueTextBox As TextBox
        Friend WithEvents SearchButton As Button
    End Class

End Namespace