﻿Imports System.Transactions
Imports NDDD.Domain.Entities
Imports NDDD.Domain.Repositories
Imports NDDD.Domain.StaticValues
Imports NDDD.Domain.ValueObjects
Imports NDDD.Infrastructure

Namespace ViewModels
    ''' <summary>
    ''' 直近値のViewModel
    ''' </summary>
    Public Class LatestViewModel
        Inherits ViewModelBase

        ''' <summary>
        ''' 計測リポジトリー
        ''' </summary>
        Private ReadOnly _measureRepository As MeasureRepository

        ''' <summary>エリアID</summary>
        Private _areaIdText As String
        ''' <summary>計測値</summary>
        Private _mesureValueText As String
        ''' <summary>計測日時</summary>
        Private _measureDateText As String

        ''' <summary>エリアID</summary>
        Public Property AreaIdText() As String
            Get
                Return _areaIdText
            End Get
            Set(value As String)
                SetProperty(_areaIdText, value)
            End Set
        End Property

        ''' <summary>計測日時</summary>
        Public Property MeasureDateText() As String
            Get
                Return _measureDateText
            End Get
            Set(value As String)
                SetProperty(_measureDateText, value)
            End Set
        End Property

        ''' <summary>計測値</summary>
        Public Property MeasureValueText() As String
            Get
                Return _mesureValueText
            End Get
            Set(value As String)
                SetProperty(_mesureValueText, value)
            End Set
        End Property

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        Public Sub New()
            Me.New(Factories.CreateMeasure)
        End Sub

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="measureRepository">計測リポジトリー</param>
        Public Sub New(measureRepository As IMeasureRepository)
            Me._measureRepository = New MeasureRepository(measureRepository)
        End Sub

        ''' <summary>
        ''' 検索
        ''' </summary>
        Public Sub Search()
            Dim measure = _measureRepository.GetLatest()
            'Dim measure = Measures.GetLatest(New AreaId(20))
            AreaIdText = measure.AreaId.DisplayValue
            MeasureDateText = measure.MeasureDate.DisplayValue
            MeasureValueText = measure.MeasureValue.DisplayValue
        End Sub

        ''' <summary>
        ''' 保存
        ''' </summary>
        Public Sub Save()
            Using scope As New TransactionScope
                ' ヘッダー
                ' 明細
                ' 在庫
                ' 履歴
                ' 顧客情報
                scope.Complete()
            End Using
        End Sub
    End Class

End Namespace