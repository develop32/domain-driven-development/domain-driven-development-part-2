﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Namespace ViewModels
    ''' <summary>
    ''' ViewModelの基底クラス
    ''' </summary>
    Public MustInherit Class ViewModelBase
        Implements INotifyPropertyChanged

        ''' <summary>
        ''' プロパティ変更イベント
        ''' </summary>
        Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        ''' <summary>
        ''' プロパティ設定
        ''' </summary>
        ''' <typeparam name="T">T</typeparam>
        ''' <param name="field">field</param>
        ''' <param name="value">value</param>
        ''' <param name="propertyName">propertyName</param>
        ''' <returns></returns>
        Protected Function SetProperty(Of T)(ByRef field As T,
                                             ByVal value As T,
                                             <CallerMemberName> ByVal Optional propertyName As String = Nothing) As Boolean
            If Equals(field, value) Then
                Return False
            End If

            field = value
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
            Return True
        End Function

        ''' <summary>
        ''' システム日付
        ''' </summary>
        ''' <returns></returns>
        Public Overridable Function GetDateTime() As DateTime
            Return DateTime.Now
        End Function

        ''' <summary>
        ''' OnPropertyChanged
        ''' </summary>
        Public Sub OnPropertyChanged()
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(""))
        End Sub
    End Class

End Namespace