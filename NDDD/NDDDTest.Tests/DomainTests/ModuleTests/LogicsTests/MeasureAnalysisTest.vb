﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports NDDD.Domain

<TestClass()> Public Class MeasureAnalysisTest

    <TestMethod()> Public Sub TestMethod1()
        Assert.AreEqual(3D, MeasureAnalysis.Execute(1, 2, 3, 4, 5))
    End Sub

End Class