﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Moq
Imports NDDD.Domain.Entities
Imports NDDD.Domain.Repositories
Imports NDDD.WinForm.ViewModels

<TestClass()> Public Class LatestViewModelTest

    <TestMethod()> Public Sub シナリオ()
        ' ========================================
        '  前準備
        ' ========================================
        Dim measureMock As New Mock(Of IMeasureRepository)
        Dim entity As New MeasureEntity(1,
                                        Convert.ToDateTime("2012/12/12 12:34:56"),
                                        12.341D)
        measureMock.Setup(Function(x) x.GetLatest()).Returns(entity)

        Dim vm As New LatestViewModel(measureMock.Object)

        ' ========================================
        '  検索ボタン押下
        ' ========================================
        vm.Search()
        ' エリアID
        Assert.AreEqual("0001", vm.AreaIdText)
        ' 計測日時
        Assert.AreEqual("2012/12/12 12:34:56", vm.MeasureDateText)
        ' 計測値
        Assert.AreEqual("12.3410℃", vm.MeasureValueText)
    End Sub

End Class