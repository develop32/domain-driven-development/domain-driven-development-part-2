﻿Imports NDDD.Domain
Imports NDDD.Domain.Repositories
Imports NDDD.Infrastructure.Fake
Imports NDDD.Infrastructure.SqlServer

''' <summary>
''' ファクトリー
''' 他のレポジトリも含めたほうが良い
''' </summary>
Public Class Factories
    ''' <summary>
    ''' 計測リポジトリーの作成
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function CreateMeasure() As IMeasureRepository
#If DEBUG Then
        If [Shared].IsFake Then Return New MeasureFake
#End If
        Return New MeasureSqlServer
    End Function
End Class
