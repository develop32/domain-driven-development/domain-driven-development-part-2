﻿Imports NDDD.Domain.Entities
Imports NDDD.Domain.Repositories

Namespace SqlServer

    ''' <summary>
    ''' 計測SqlServer
    ''' </summary>
    Friend NotInheritable Class MeasureSqlServer
        Implements IMeasureRepository

        ''' <summary>
        ''' 直近値の取得
        ''' </summary>
        ''' <returns></returns>
        Public Function GetLatest() As MeasureEntity Implements IMeasureRepository.GetLatest
            Throw New NotImplementedException()
        End Function

        ''' <summary>
        ''' エリアごとの直近値取得
        ''' </summary>
        ''' <returns></returns>
        Public Function GetLatests() As IReadOnlyList(Of MeasureEntity) Implements IMeasureRepository.GetLatests
            Throw New NotImplementedException()
        End Function
    End Class

End Namespace