﻿Imports System.IO
Imports NDDD.Domain
Imports NDDD.Domain.Entities
Imports NDDD.Domain.Exceptions
Imports NDDD.Domain.Repositories

Namespace Fake
    ''' <summary>
    ''' 計測Fake
    ''' </summary>
    Friend NotInheritable Class MeasureFake
        Implements IMeasureRepository

        ''' <summary>
        ''' 直近値の取得
        ''' </summary>
        ''' <returns></returns>
        Public Function GetLatest() As MeasureEntity Implements IMeasureRepository.GetLatest
            Try
                Dim lines() As String = File.ReadAllLines([Shared].FakePath)
                Dim value() As String = lines(0).Split(","c)
                Return New MeasureEntity(Convert.ToInt32(value(0)),
                                         Convert.ToDateTime(value(1)),
                                         Convert.ToDecimal(value(2)))
            Catch ex As Exception
                'Return New MeasureEntity(10,
                '                         Convert.ToDateTime("2020/12/12 12:34:56"),
                '                         123.341D)
                Throw New FakeException("MeasureFakeの取得に失敗しました。", ex)
            End Try
        End Function

        ''' <summary>
        ''' エリアごとの直近値取得
        ''' </summary>
        ''' <returns></returns>
        Public Function GetLatests() As IReadOnlyList(Of MeasureEntity) Implements IMeasureRepository.GetLatests
            Dim result As New List(Of MeasureEntity) From {
                New MeasureEntity(10, New Date(2020, 12, 12, 12, 34, 56), 123.341D),
                New MeasureEntity(20, New Date(2020, 12, 12, 12, 34, 56), 223.341D),
                New MeasureEntity(30, New Date(2020, 12, 12, 12, 34, 56), 323.341D)
            }
            Return result
        End Function
    End Class

End Namespace